Installation
============

Pip installation
----------------

.. code:: bash

    pip install bingus

Poetry installation
-------------------

.. code:: bash

    poetry add bingus