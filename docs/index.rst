.. bingus documentation master file, created by
   sphinx-quickstart on Fri Aug 26 20:54:18 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bingus's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   modules

.. automodule:: bingus
    :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


