# Bingus: the package
[![Rate this package](https://badges.openbase.com/python/rating/bingus.svg?token=2Z+v2lV+4IFUd0PoL2JC2J1uCi8wSME/Tyys2XoKoMU=)](https://openbase.com/python/bingus?utm_source=embedded&amp;utm_medium=badge&amp;utm_campaign=rate-badge)

Bingus is a library of utility scripts and functions.

Documentation can be found [here](https://bingus.readthedocs.io).

## Generate Documentation

You can generate the documentation running `make html` in the `docs` directory.

1. `cd docs`
2. `make html`

Then open the documentation by opening `./docs/_build/html/index.html` in your browser of
choice ([ideally Firefox](https://itsfoss.com/why-firefox/)).