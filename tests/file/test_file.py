import os
import shutil

from pytest import fixture

import bingus


@fixture
def clean_directory():
    """Fixture that yields the path to a clean empty directory.

    Yields:
        str: Path to an empty directory.

    """
    temp_dir = os.path.join(os.getcwd(), "temp")
    if os.path.exists(temp_dir):
        shutil.rmtree(temp_dir)
    os.makedirs(temp_dir)
    yield temp_dir
    if os.path.exists(temp_dir):
        shutil.rmtree(temp_dir)


def test_ensure_dirs_exists_creates_multiple_dirs(clean_directory):
    multiple_dir_path = os.path.join(clean_directory, "dir1", "dir2", "dir3")
    bingus.file.ensure_dirs_exists(multiple_dir_path)

    assert os.path.exists(multiple_dir_path)


def test_ensure_dirs_exists_ignores_existing_dir(clean_directory):
    multiple_dir_path = os.path.join(clean_directory, "dir1", "dir2", "dir3")
    os.makedirs(multiple_dir_path)
    file_path = os.path.join(multiple_dir_path, "file.txt")
    expected_file_content = "Hello, World!"
    with open(file_path, "w") as f:
        f.write(expected_file_content)

    bingus.file.ensure_dirs_exists(multiple_dir_path)

    assert os.path.exists(multiple_dir_path)
    assert os.path.exists(file_path)

    with open(file_path, "r") as f:
        actual_file_content = f.read()
    assert actual_file_content == expected_file_content
