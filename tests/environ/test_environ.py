import os

import pytest
from pytest import fixture

import bingus.environ
from bingus.environ.exceptions import EmptyEnvironmentVariable, MissingEnvironmentVariable


@fixture
def clean_env_var_name():
    clean_env_var = "BINGUS_TEST_ENVIRONMENT_VAR_ABCD"
    if clean_env_var in os.environ:
        del os.environ[clean_env_var]
    yield clean_env_var
    if clean_env_var in os.environ:
        del os.environ[clean_env_var]


def test_require_rejects_empty_env_var(clean_env_var_name):
    os.environ[clean_env_var_name] = ""
    with pytest.raises(EmptyEnvironmentVariable) as e_info:
        value = bingus.environ.require(clean_env_var_name)

    assert bingus.environ.require(clean_env_var_name, allow_empty=True) == ""


def test_require_rejects_missing_env_var(clean_env_var_name):
    with pytest.raises(MissingEnvironmentVariable) as e_info:
        bingus.environ.require(clean_env_var_name)

    try:
        bingus.environ.require(clean_env_var_name)
    except MissingEnvironmentVariable as exc:
        assert str(exc) == f"Environment variable {clean_env_var_name} is not set."


def test_require_accepts_valid_env_var(clean_env_var_name):
    expected_value = "bingus is a cat"
    os.environ[clean_env_var_name] = expected_value
    actual_value = bingus.environ.require(clean_env_var_name)
    assert actual_value == expected_value
